#include <SFML/Graphics.hpp>
#include <SFML/Audio.hpp>
#include <iostream>

int main()
{
	sf::RenderWindow window(sf::VideoMode(800, 600), "ola q ase");
	sf::Texture texture;
	//sf::Texture texture2;
	texture.loadFromFile("Images/NyanCatAnimation.png");
	//texture2.loadFromFile("Images/gatoMalo.png");

	sf::IntRect rectSourceSprite(0, 0, 114, 76);
	sf::Sprite player(texture, rectSourceSprite);
	/*sf::Sprite enemy[7];
	for (int i = 0; i < 7; i++)
	{
		enemy[i] = sf::Sprite(texture2);
		enemy[i].setPosition(0, 0);
	}*/
	
	sf::Clock clock,clock2;
	sf::Time time;
	float elapsed;

	const float speed_X = 250.f;
	const float speed_Y = 200.f;
	sf::Music music;
	if (!music.openFromFile("Sound/Nyan.ogg"))
		std::cout << "Error" << std::endl;
	music.play();


	while (window.isOpen())
	{
		sf::Event event;
		while (window.pollEvent(event))
		{
			if (event.type == sf::Event::Closed || sf::Keyboard::isKeyPressed(sf::Keyboard::Escape))
				window.close();
		}
		time = clock.getElapsedTime();
		elapsed = time.asSeconds();

		//move
		if (sf::Keyboard::isKeyPressed(sf::Keyboard::Right))
			player.move(speed_X * elapsed, 0);
		if (sf::Keyboard::isKeyPressed(sf::Keyboard::Left))
			player.move(speed_X * -1 * elapsed, 0);
		if (sf::Keyboard::isKeyPressed(sf::Keyboard::Down))
			player.move(0, speed_Y*elapsed);
		if (sf::Keyboard::isKeyPressed(sf::Keyboard::Up))
			player.move(0, speed_Y* -1 * elapsed);

		//animation
		if (clock2.getElapsedTime().asSeconds()>0.15f)
		{
			if (rectSourceSprite.left == 342)
				rectSourceSprite.left = 0;
			else
				rectSourceSprite.left += 114;
			player.setTextureRect(rectSourceSprite);
			clock2.restart();
		}

		clock.restart();
		//DISPLAY
		window.clear();
		window.draw(player);
		/*for (int i = 0; i < 7; i++)
		{
			window.draw(enemy[i]);
		}*/
		window.display();
	}

	return 0;
}